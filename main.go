package main

// Quotes are taken from
// http://pubs.opengroup.org/onlinepubs/9699919799/utilities/ed.html

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"regexp"
	"strconv"
	"strings"
	"unicode/utf8"
)

var sFlag = flag.Bool("s", false, "suppress byte count output")
var pFlag = flag.String("p", "", "set prompt string")
var dFlag = flag.Bool("d", false, "debug")

var exitStatus int

func main() {
	// Avoid circular initialisation problem.
	Commands = map[string]Descriptor{
		"":  {"", Null, []Address{{kind: A_dot, n: 1}}, false},
		"a": {"a", Append, []Address{{kind: A_dot}}, true},
		"d": {"d", Delete,
			[]Address{{kind: A_dot}, {kind: A_dot}}, true},
		"e": {"e", Edit, nil, false},
		"f": {"f", Filename, nil, false},
		"h": {"h", Help, nil, false},
		"H": {"H", HelpMode, nil, false},
		"k": {"k", Mark, []Address{{kind: A_dot}}, false},
		"n": {"n", Number,
			[]Address{{kind: A_dot}, {kind: A_dot}}, false},
		"p": {"p", Print,
			[]Address{{kind: A_dot}, {kind: A_dot}}, false},
		"P": {"P", Prompt, nil, false},
		"q": {"q", Quit, nil, false},
		"Q": {"Q", QuitWithoutChecking, nil, false},
		"r": {"r", Read, []Address{{kind: A_dollar}}, true},
		"u": {"u", Undo, nil, false},
		"w": {"w", Write,
			[]Address{{kind: A_n, n: 1}, {kind: A_dollar}}, false},
		"=": {"=", LineNumber, []Address{{kind: A_dollar}}, false},
	}

	flag.Parse()

	args := flag.Args()
	if len(args) > 1 {
		log.Fatal("too many file arguments")
	}

	run(args)
	os.Exit(exitStatus)
}

type Error struct {
	s string
}

func (e *Error) Error() string {
	return "ed: " + e.s
}

// Ed models an entire editor:
// a buffer, a source of commands, and various remembered state.
type Ed struct {
	// The (line) buffer is implemented as two slices:
	// `l`, a left part, and `r`, a right part,
	// with a (conceptual) gap between them.
	// The Gap() function moves the gap.
	// The gap is the place where inserting and deleting is easy.
	l, r []Line
	// Current line number.
	current int

	// The command source
	reader         *bufio.Reader
	prompt_enabled bool
	prompt         string

	// The remembered filename
	pathname string
	// Many commands use this to avoid accidental buffer overwrite
	modified     bool
	last_command string

	helpful     bool
	explanation string
	error       bool

	undo func()
}

type Line struct {
	s    string
	mark uint32
}

// Issue a prompt if enabled.
func (ed *Ed) Prompt() {
	// "If the -p option is specified, the prompt string
	//  shall be written to standard output before each command
	//  is read"
	if ed.prompt_enabled {
		fmt.Print(ed.prompt)
	}
}

// Number of lines in buffer.
func (ed *Ed) N() int {
	return len(ed.l) + len(ed.r)
}

// Return the string for line n (where first line is n=1)
func (ed *Ed) Line(n int) string {
	return ed.LineRef(n).s
}

// Return the a reference to line n (where first line is n=1);
// The reference has a very short validity and
// should not be used after the buffer is modified or
// the gap is moved.
func (ed *Ed) LineRef(n int) *Line {
	// beware off-by-one errors
	if n <= len(ed.l) {
		return &ed.l[n-1]
	}
	n -= len(ed.l)
	return &ed.r[n-1]
}

// Put a string in the gap (appending it to the left part).
func (ed *Ed) AppendGap(s string) {
	ed.l = append(ed.l, Line{s: s})
}

// Rearrange lines so that
// there are n lines to the left of the gap.
// After the call, len(ed.l) will be n.
func (ed *Ed) Gap(n int) {
	if len(ed.l) == n {
		return
	}
	ll := len(ed.l)
	lr := len(ed.r)
	if ll+lr < n {
		log.Fatal("Not enough lines", ll, lr, n)
	}

	d := ll - n
	if ll < n {
		// Wind gap forwards, moving lines from r to l
		d = -d
		ed.l = append(ed.l, ed.r[:d]...)
		ed.r = ed.r[d:]
	} else {
		// Create a fresh slice, to avoid aliasing problems
		temp := append([]Line{}, ed.l[ll-d:]...)
		ed.r = append(temp, ed.r...)
		ed.l = ed.l[:ll-d]
	}
	if len(ed.l) != n {
		log.Fatal("Failed in Gap()")
	}
}

// Seek the line with the mark.
func (ed *Ed) SeekMark(k uint) (int, bool) {
	bit := uint32(1) << k
	for i := 1; i <= ed.N(); i += 1 {
		if (ed.LineRef(i).mark & bit) != 0 {
			return i, true
		}
	}
	return 0, false
}

// Set mark k on line n
func (ed *Ed) SetMark(k uint, n int) {
	l := ed.LineRef(n)
	l.mark |= 1 << k
}

// Search forwards from the next line, wrapping around.
// Returns either (line number, nil) or (0, error).
func (ed *Ed) WindForward(re *regexp.Regexp) (int, error) {
	s := ed.current
	if s < 1 {
		return 0, &Error{"This is an empty buffer"}
	}

	i := s
	for {
		if i == ed.N() {
			i = 1
		} else {
			i += 1
		}
		if re.MatchString(ed.Line(i)) {
			return i, nil
		}
		if i == s {
			return 0, &Error{"Match missed"}
		}
	}
}

// Search backwards from the previous line, wrapping around.
// Returns either (line number, nil) or (0, error).
func (ed *Ed) WindBackward(re *regexp.Regexp) (int, error) {
	s := ed.current
	if s < 1 {
		return 0, &Error{"This is an empty buffer"}
	}

	i := s
	for {
		if i == 1 {
			i = ed.N()
		} else {
			i -= 1
		}
		if re.MatchString(ed.Line(i)) {
			return i, nil
		}
		if i == s {
			return 0, &Error{"Match missed"}
		}
	}
}

// Explain a problem.
func (ed *Ed) Explain(e string) {
	fmt.Println("?")
	ed.explanation = e
	if ed.helpful {
		fmt.Println(ed.explanation)
	}

}

// Exit (the system!)
func (ed *Ed) Exit() {
	status := 0
	if ed.error {
		status = 1
	}
	os.Exit(status)
}

// Update remembered pathname (if not empty),
// returning the remembered pathname.
func (ed *Ed) Pathname(pathname string) string {
	if pathname != "" {
		ed.pathname = pathname
	}
	return ed.pathname
}

// Return a function that
// restores the current line to its value now.
func deferRestoreCurrent(ed *Ed) func() {
	o := ed.current
	return func() {
		ed.current = o
	}
}

// Functions to implement commands,
// presented in same order as spec.

func Append(ed *Ed, instruction *Instruction, a, b int) {
	ed.Gap(b)

	for _, app := range instruction.x {
		ed.AppendGap(app)
	}

	c := b + len(instruction.x)

	setCurrent := deferRestoreCurrent(ed)
	if len(instruction.x) > 0 {
		undo := func() {
			Delete(ed, nil, b+1, c)
			setCurrent()
		}
		ed.undo = undo
	} else {
		ed.undo = setCurrent
	}

	ed.current = c
}

func Delete(ed *Ed, instruction *Instruction, a, b int) {
	if a > b {
		ed.Explain("addresses asorted")
		return
	}
	ed.Gap(b)

	deletia := append([]Line{}, ed.l[a-1:]...)
	deleted_strings := []string{}

	ed.l = ed.l[:a-1]

	setCurrent := deferRestoreCurrent(ed)

	undo := func() {
		for _, l := range deletia {
			deleted_strings = append(deleted_strings, l.s)
		}
		Append(ed, &Instruction{x: deleted_strings}, a-1, a-1)
		setCurrent()

		// restore deleted marks
		for i, l := range deletia {
			for k := uint(0); k < 32; k += 1 {
				if ((1 << k) & l.mark) == 0 {
					continue
				}
				_, marked := ed.SeekMark(k)
				if marked {
					continue
				}
				ed.SetMark(k, a+i)
			}
		}
	}
	ed.undo = undo

	ed.current = len(ed.l) + 1
	if ed.current > ed.N() {
		ed.current = ed.N()
	}
}

func Edit(ed *Ed, instruction *Instruction, a, b int) {
	if ed.modified && ed.last_command != "e" {
		ed.Explain("edits unwritten, edit forbidden")
		return
	}

	ed.l = nil
	ed.r = nil
	ed.current = 0

	pathname := ed.Pathname(instruction.t)

	implementRead(ed, 0, pathname)
	ed.modified = false
}

func Filename(ed *Ed, instruction *Instruction, a, b int) {
	pathname := ed.Pathname(instruction.t)
	fmt.Println(pathname)
}

func Help(ed *Ed, instruction *Instruction, a, b int) {
	if ed.explanation != "" {
		fmt.Println(ed.explanation)
	}
}

func HelpMode(ed *Ed, instruction *Instruction, a, b int) {
	ed.helpful = !ed.helpful
	if ed.helpful {
		Help(ed, instruction, a, b)
	}
}

func Mark(ed *Ed, instruction *Instruction, a, b int) {
	r := instruction.t[0]
	k := uint(r) & 0x1f

	// Clear existing mark.
	n, ok := ed.SeekMark(k)
	if ok {
		l := ed.LineRef(n)
		l.mark &^= 1 << k
	}

	ed.SetMark(k, b)
}

func Number(ed *Ed, instruction *Instruction, a, b int) {
	for i := a; i <= b; i += 1 {
		fmt.Printf("%d\t%s\n", i, ed.Line(i))
	}
	ed.current = b
}

func Print(ed *Ed, instruction *Instruction, a, b int) {
	for i := a; i <= b; i += 1 {
		fmt.Println(ed.Line(i))
	}
	ed.current = b
}

func Prompt(ed *Ed, instruction *Instruction, a, b int) {
	ed.prompt_enabled = !ed.prompt_enabled
}

func Quit(ed *Ed, instruction *Instruction, a, b int) {
	if ed.modified && ed.last_command != "q" {
		ed.Explain("edits unwrit, cannot quit")
		return
	}
	ed.Exit()
}

func QuitWithoutChecking(ed *Ed, instruction *Instruction, a, b int) {
	ed.Exit()
}

func Read(ed *Ed, instruction *Instruction, a, b int) {
	pathname := ed.Pathname(instruction.t)
	if pathname == "" {
		ed.Explain("reckless read")
		return
	}

	implementRead(ed, b, pathname)
}

func implementRead(ed *Ed, line int, pathname string) {
	ed.Gap(line)

	in, err := os.Open(pathname)
	if err != nil {
		ed.Explain(fmt.Sprintf("system says: %s", err))
		ed.error = true
		return
	}
	defer in.Close()

	scanner := bufio.NewScanner(in)
	bytes := 0
	n := 0
	for scanner.Scan() {
		l := scanner.Text()
		ed.AppendGap(l)
		bytes += len(l) + 1
		n += 1
	}
	ed.current = line + n

	if err := scanner.Err(); err != nil {
		ed.Explain(fmt.Sprintf("%s", err))
		ed.error = true
	}

	if !*sFlag {
		fmt.Printf("%d\n", bytes)
	}

}

func Undo(ed *Ed, instruction *Instruction, a, b int) {
	ed.undo()
}

func Write(ed *Ed, instruction *Instruction, a, b int) {
	pathname := ed.Pathname(instruction.t)
	if pathname == "" {
		ed.Explain("write whither?")
		return
	}
	out, err := os.OpenFile(pathname,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		ed.Explain(fmt.Sprint(err))
		return
	}
	defer out.Close()

	bytes := 0
	for i := a; i <= b; i += 1 {
		l := ed.Line(i)
		fmt.Fprintln(out, l)
		bytes += len(l) + 1
	}

	if a == 1 && b == ed.N() {
		ed.modified = false
	}
	if !*sFlag {
		fmt.Printf("%d\n", bytes)
	}
}

func LineNumber(ed *Ed, instruction *Instruction, a, b int) {
	fmt.Printf("%d\n", b)
}

func Null(ed *Ed, instruction *Instruction, a, b int) {
	ed.current = b
	fmt.Println(ed.Line(b))
}

const (
	A_default addrKind = iota
	A_dot
	A_dollar
	A_n
	A_tick
	A_forward
	A_backward
)

type addrKind int

// An address.
type Address struct {
	kind  addrKind // one of A_invalid to A_semicolon
	n     int
	r     string
	semic bool
}

// Returns an address (and nil); or,
// 0 and an error.
// cmd is used to determine if the address 0 is valid.
func address_evaluate(
	ed *Ed, cmd string, address Address, defalt int,
) (int, error) {
	var n int
	min := 1
	if ZeroValid[cmd] {
		min = 0
	}

	switch address.kind {
	case A_n:
		n = address.n
	case A_default:
		n = defalt
	case A_dollar:
		n = ed.N()
		n += address.n
	case A_dot:
		n = ed.current
		n += address.n
	case A_tick:
		r := address.r[1]
		k := uint(r) & 0x1f
		l, ok := ed.SeekMark(k)
		if !ok {
			return 0, &Error{"mark " + address.r + " not found"}
		}
		n = l
		n += address.n
	case A_forward:
		re, err := regexp.Compile(address.r[1:])
		if err != nil {
			return 0, err
		}
		n, err = ed.WindForward(re)
		if err != nil {
			return 0, err
		}
		n += address.n
	case A_backward:
		re, err := regexp.Compile(address.r[1:])
		if err != nil {
			return 0, err
		}
		n, err = ed.WindBackward(re)
		if err != nil {
			return 0, err
		}
		n += address.n
	default:
		log.Fatal("unhandled kind ", address)
	}
	if n < min {
		return 0, &Error{fmt.Sprintf("address evaluates to %d which is less than the minimum permitted address %d",
			n, min)}
	}
	if n > ed.N() {
		return 0, &Error{fmt.Sprintf("address evaluates to %d which is more than the largest line number %d", n, ed.N())}
	}
	if address.semic {
		ed.current = n
	}
	return n, nil
}

// Those commands for which 0 is a valid address (see address_evaluate)
var ZeroValid = map[string]bool{
	"a": true, "c": true,
	"i": true, "r": true,
	"=": true,
}

type tokenType int

const (
	tokenDot tokenType = iota
	tokenDollar
	tokenPlus
	tokenMinus
	tokenNumber
	tokenBRE
	tokenTick
	tokenComma
	tokenSemicolon
	tokenEOF
)

type Token struct {
	typ tokenType
	val string
}

type Lexer struct {
	input  string
	start  int
	pos    int
	width  int
	tokens chan Token
}

func lex(input string) (*Lexer, chan Token) {
	l := &Lexer{
		input:  input,
		tokens: make(chan Token),
	}
	go l.run()
	return l, l.tokens
}

func (l *Lexer) emit(typ tokenType) {
	l.tokens <- Token{typ, l.input[l.start:l.pos]}
	l.start = l.pos
}

func (l *Lexer) run() {
	for state := lexInit; state != nil; {
		state = state(l)
	}
	close(l.tokens)
}

type stateFunction func(*Lexer) stateFunction

const eof = -1

func lexInit(l *Lexer) stateFunction {
	for {
		switch r := l.next(); {
		case r == eof:
			l.emit(tokenEOF)
			return nil
		case isSpace(r):
			l.ignore()
		case r == '.':
			l.emit(tokenDot)
		case r == '$':
			l.emit(tokenDollar)
		case r == '+' || r == '-' || '0' <= r && r <= '9':
			l.backup()
			return lexNumber
		case r == '/':
			return lexBREForward
		case r == '?':
			return lexBREBackward
		case r == '\'':
			return lexTick
		case r == ',':
			l.emit(tokenComma)
		case r == ';':
			l.emit(tokenSemicolon)
		default:
			l.backup()
			l.emit(tokenEOF)
			return nil
		}
	}
}

func isSpace(c rune) bool {
	return c == ' ' || c == '\t'
}

func (l *Lexer) acceptRun(valid string) {
	for strings.IndexRune(valid, l.next()) >= 0 {
	}
	l.backup()
}

func lexNumber(l *Lexer) stateFunction {
	signed := false
	s := l.next()
	switch s {
	case '+', '-':
		signed = true
	default:
		l.backup()
	}
	l.acceptRun("0123456789")

	if l.pos == l.start+1 && signed {
		if s == '+' {
			l.emit(tokenPlus)
		} else {
			l.emit(tokenMinus)
		}
	} else {
		l.emit(tokenNumber)
	}
	return lexInit
}

func lexBREForward(l *Lexer) stateFunction {
	return lexBRE(l, '/')
}
func lexBREBackward(l *Lexer) stateFunction {
	return lexBRE(l, '?')
}

func lexBRE(l *Lexer, delim rune) stateFunction {
	for {
		s := l.next()
		if s == eof {
			l.emit(tokenBRE)
			break
		}
		if s == delim {
			l.backup()
			l.emit(tokenBRE)
			l.next()
			l.ignore()
			break
		}
		if s == '\\' {
			s = l.next()
		}
	}
	return lexInit
}

func lexTick(l *Lexer) stateFunction {
	l.next()
	l.emit(tokenTick)
	return lexInit
}

func (l *Lexer) next() (r rune) {
	if l.pos >= len(l.input) {
		l.width = 0
		return eof
	}
	r, l.width = utf8.DecodeRuneInString(l.input[l.pos:])
	l.pos += l.width
	return r
}

func (l *Lexer) ignore() {
	l.start = l.pos
}

func (l *Lexer) backup() {
	l.pos -= l.width
}

func (l *Lexer) remainder() string {
	return l.input[l.pos:]
}

// Table from http://pubs.opengroup.org/onlinepubs/9699919799/utilities/ed.html
// source  result
// ,       1,$
// ,addr   1,addr
// addr,   addr,addr
// ;       .;$
// ;addr   .;addr
// addr;   addr;addr
// According to this table,
// the left of comma always defaults to 1;
// the left of semicolon always defaults to dot.

func addresses_parse(s string) (as []Address, c string) {
	var addresses []Address
	var address Address
	initial := true
	lexer, tokens := lex(s)
	// push (append) the address onto the list.
	push := func() {
		addresses = append(addresses, address)
		address = Address{}
		initial = true
	}
	for token := range tokens {
		switch token.typ {
		case tokenDot:
			if !initial {
				log.Fatal("bad dot " + s)
			}
			address.kind = A_dot
			initial = false
		case tokenDollar:
			if !initial {
				log.Fatal("bad dollar " + s)
			}
			address.kind = A_dollar
			initial = false
		case tokenPlus:
			if initial {
				address.kind = A_dot
			}
			address.n += 1
			initial = false
		case tokenMinus:
			if initial {
				address.kind = A_dot
			}
			address.n -= 1
			initial = false
		case tokenNumber:
			if initial {
				if token.val[0] != '+' && token.val[0] != '-' {
					address.kind = A_n
				} else {
					address.kind = A_dot
				}
			}
			i, _ := strconv.Atoi(token.val)
			address.n += i
			initial = false
		case tokenBRE:
			if token.val[0] == '/' {
				address.kind = A_forward
			} else {
				address.kind = A_backward
			}
			address.r = token.val
			initial = false
			re, err := regexp.Compile(address.r)
			_ = re
			_ = err
		case tokenTick:
			address.kind = A_tick
			address.r = token.val
			initial = false
		case tokenComma:
			push()
		case tokenSemicolon:
			address.semic = true
			push()
		case tokenEOF:
			push()
			break
		}
	}

	if *dFlag {
		fmt.Println("Addresses", addresses)
	}

	if len(addresses) == 1 &&
		addresses[0].kind == A_default {
		addresses = []Address{}
	}

	if len(addresses) >= 2 {
		// Check for default,default|EOF
		// and INIT|default,default
		if addresses[0].kind == A_default &&
			addresses[1].kind == A_default {
			if addresses[0].semic {
				addresses[0].kind = A_dot
			} else {
				addresses[0].kind = A_n
				addresses[0].n = 1
			}
			addresses[1].kind = A_dollar
		}
	}

	return addresses, lexer.remainder()
}

// Gather lines from reader (for a c).
func gather_input(reader *bufio.Reader) []string {
	var lines []string
	for {
		text, err := reader.ReadString('\n')
		if err == io.EOF {
			return lines
		}
		if err != nil {
			log.Fatal(err)
		}
		text = strings.TrimSuffix(text, "\n")
		if text == "." {
			return lines
		}
		lines = append(lines, text)
	}
}

// This is the description of a class of commands;
// for example, the "a" instance describes all "a" commands.
type Descriptor struct {
	letter   string
	execute  func(*Ed, *Instruction, int, int)
	def      []Address // default
	modifies bool
}

// Number of addresses used by command.
func (descriptor *Descriptor) AddressCount() int {
	return len(descriptor.def)
}

var Commands map[string]Descriptor

// This represents a command instruction compiled from the input.
// It includes the addresses used and additional parameters:
// text input to append, regular expressions, file names.
type Instruction struct {
	command string    // used as index into Commands
	a       []Address // addresses (could be > 2)
	x       []string  // Additional material for a c and so on.
	t       string    // Tail: pathname in e f r w; mark for k.
}

// Compile an instruction from the input.
// Returns (&instruction, "") or (nil, message),
// where message is an error explanation to be displayed to the user.
func Compile(text string, reader *bufio.Reader) (
	*Instruction, string,
) {
	text = strings.TrimSpace(text)
	addresses, tail := addresses_parse(text)
	var opcode string
	if tail != "" {
		opcode = tail[:1]
		tail = tail[1:]
	}
	descriptor, ok := Commands[opcode]
	if !ok {
		return nil, "command calumny: " + opcode
	}

	if descriptor.AddressCount() == 0 &&
		len(addresses) > 0 {
		// "If more than the required number of
		// addresses are provided to a command that
		// requires zero addresses, it shall be an
		// error."
		return nil, "one command, zero addresses"
	}
	if len(addresses) == 0 {
		addresses = descriptor.def
	}

	var instruction Instruction
	instruction.command = opcode
	instruction.a = addresses
	switch opcode {
	case "a":
		appendix := gather_input(reader)
		instruction.x = appendix
	case "e", "f", "k", "r", "w":
		instruction.t = strings.TrimLeft(tail, " ")
	}
	return &instruction, ""
}

func Execute(ed *Ed, instruction *Instruction) {
	addresses := instruction.a
	descriptor := Commands[instruction.command]

	var a int
	var b int
	var err error

	// Evaluate all the addresses,
	// saving the last two.
	// "if more than the required number of
	// addresses are provided to a command, the
	// addresses specified first shall be evaluated
	// and then discarded until the maximum number of
	// valid addresses remain"
	for i, address := range addresses {
		command := ""
		if i == len(addresses)-1 {
			command = instruction.command
		}
		a = b
		b, err = address_evaluate(ed, command, address, a)
		if err != nil {
			ed.Explain(fmt.Sprint(err))
			return
		}
		if i == 0 {
			a = b
		}
	}

	descriptor.execute(ed, instruction, a, b)
	if descriptor.modifies {
		ed.modified = true
	}
	ed.last_command = descriptor.letter
}

func loop(ed *Ed) {
	ed.reader = bufio.NewReader(os.Stdin)
	for {
		ed.Prompt()
		text, err := ed.reader.ReadString('\n')
		if err == io.EOF {
			text = "q"
		} else if err != nil {
			log.Fatal(err)
		}

		instruction, explanation := Compile(text, ed.reader)
		if explanation != "" {
			ed.Explain(explanation)
			continue
		}

		Execute(ed, instruction)
	}
}

func run(args []string) {
	ed := Ed{}
	ed.prompt = "*"

	if len(*pFlag) > 0 {
		ed.prompt_enabled = true
		ed.prompt = *pFlag
	}

	if len(args) > 0 {
		var instruction Instruction
		instruction.command = "r"
		instruction.a = Commands["r"].def
		filename := args[0]
		instruction.t = filename
		Execute(&ed, &instruction)
		ed.modified = false
	}

	loop(&ed)
}
