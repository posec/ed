# ed

_ed_ is the standard text editor.

## Development

For tests, install `urchin` with

    npm install urchin

then

    make test

or

    sh test.sh

to run tests.

## TODO

    c
    E
    g G
    i
    j
    l
    m
    s
    t
    u (for some commands)
    v V
    ! shell escape
    ! for r w etc
    correct exit status


## Model

- current file (set when opening file), can be empty
- current line (number)
- buffer changed (since last write)
- previous command (so that q then q works)
- up to 26 marks, a-z; each associated with a specific line
- (during execution of `g`) each line may have a mark
- the last BRE encountered (for `//` and so on)
- the previous replacement (for `%` in `s` command)
- undo state (a sequence of commands)
- help mode
- previous explanation (help string)
- prompt enabled
- prompt string

## Specification Problems

http://pubs.opengroup.org/onlinepubs/9699919799/utilities/ed.html

The `f` command is not specified when there is no remembered
pathname. In particular, is this an error?

The `h` command does not specify the output when there is
no most recent '?' notification.

On an empty buffer, the default addresses may be meaningless.
Example: `,p`. On GNU ed this is valid and prints nothing.
On `posec/ed` and v7 `ed` it is an error.

The `r` command does not specify what the current line
should be set to in the case where no lines are read.

The `w` command specifies that the default address is `1,$`
which is an invalid address in the case of an empty buffer.
This seems unlikely to be intended.
(Ubuntu `ed` writes out an empty file; Version 7 `ed` complains)

Address evaluation has an unclear evaluation order.
Example: in an empty file, address 4 will be invalid.
In a command like `4a`
is it permitted to gather the lines to be appended,
then complain that the address is invalid?
In v7 `ed` and GNU `ed` an error is raised about the address
before the text is gathered.
In `posec/ed` text is gathered then a problem with the address
is raised.

Implicit , and ; addresses.
What are the following addresses supposed to do:

    3;;n

    3,,n

    3;,n

    3,;n

Undoing the deletion of marked lines.
What happens to marks when you undo the deletion of lines that
were marked?
Following a conversation with twitter.com/ed1conf it was
determined that:
- this is probably undefined by the spec;
- the marks are restored; except,
- if a mark has been set between the delete and the undo,
  undo it is not restored.
